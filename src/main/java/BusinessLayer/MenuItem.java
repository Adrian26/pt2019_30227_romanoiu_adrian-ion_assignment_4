package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuItem implements Serializable {

    private String name;
    private float price;
    private List<MenuItem> menuItems;

    public MenuItem(String name){
        menuItems = new ArrayList<MenuItem>();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void computePrice(){
        float totalPrice = 0.0f;

        for (MenuItem menuItem : menuItems){
            totalPrice += menuItem.getPrice();
        }

        this.setPrice(totalPrice);
    }

    public List<MenuItem> getMenuItems() {
        return this.menuItems;
    }

    public void add(MenuItem menuItem){
        this.menuItems.add(menuItem);
        this.computePrice();
    }

    public String toString(){
        String toReturn = "";

        for (MenuItem mi : menuItems){
            toReturn += mi.getName() + " ";
        }

        return toReturn;
    }

}
