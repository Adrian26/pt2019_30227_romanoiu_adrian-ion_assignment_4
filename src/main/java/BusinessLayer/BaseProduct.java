package BusinessLayer;

public class BaseProduct extends MenuItem{

    public BaseProduct(String name, float price)
    {
        super(name);
        this.setPrice(price);
    }

}
