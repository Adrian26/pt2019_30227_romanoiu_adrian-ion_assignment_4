package BusinessLayer;


import java.util.Collection;

public interface RestaurantProcessing {

    /**
     * @pre temp != null
     * @pre temp.getPrice > 0
     * @post getSize()==getSize()@pre+1
     */
    public void add(MenuItem temp);

    /**
     * @post getSize==getSize()@pre
     * @pre index<getSize()
     */
    public void modify(Object value, int index, int attribute);

    /**
     * @pre index >= 0
     * @inv isListEmpty()
     * @post getSize()==getSize()@pre-1
     * @post getSize() > 1
     */
    public void delete(int id);

    /**
     * @pre c != null
     * @post getSize() == getSize()@pre + 1
     */
    public void createOrder(Order o, Collection<MenuItem> c);

    public void createBill(Order o);

}
