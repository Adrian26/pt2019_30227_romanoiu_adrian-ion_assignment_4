package BusinessLayer;

import java.util.Date;

public class Order {

    private int orderID;
    private Date date;
    private int tableID;

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }

    public Order(int orderID, Date date, int tableID){
        this.orderID = orderID;
        this.date = date;
        this.tableID = tableID;
    }

    @Override
    public int hashCode() {
        return orderID + tableID + date.getDay();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }

        if (obj == null || obj.getClass() != this.getClass()){
            return false;
        }

        Order order = (Order) obj;

        return !(order.hashCode() == this.hashCode());
    }

}
