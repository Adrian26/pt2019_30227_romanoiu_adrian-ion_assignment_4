package BusinessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem{

    private List<MenuItem> products;

    public CompositeProduct(String name){
        super(name);
        this.setName(name);
        this.products = new ArrayList<MenuItem>();
    }

    public void computePrice(){
        float totalPrice = 0.0f;

        for (MenuItem menuItem : products){
            totalPrice += menuItem.getPrice();
        }

        this.setPrice(totalPrice);
    }

    public String toString(){
        String toReturn = "";

        for (MenuItem mi : products){
            toReturn += mi.getName() + " ";
        }

        return toReturn;
    }

    public List<MenuItem> getMenuItems() {
        return this.products;
    }

    public void add(MenuItem menuItem){
        this.products.add(menuItem);
        this.computePrice();
    }

}
