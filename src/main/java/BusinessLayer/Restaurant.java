package BusinessLayer;

import DataLayer.FileWriter;
import DataLayer.RestaurantSerialization;

import java.util.*;



public class Restaurant extends Observable implements RestaurantProcessing{

    private List<MenuItem> menu;
    private HashMap<Order, Collection<MenuItem>> orders;

    public Restaurant(){
        this.menu = new RestaurantSerialization().deserializate();
        this.orders = new HashMap<>();
    }

    public void createOrder(Order o, Collection<MenuItem> c){
        assert c != null: "Empty order!";

        int oldSize = this.orders.size();

        this.orders.put(o, c);

        assert oldSize + 1 == this.orders.size(): "Order not created!";

        String s = "I'm cooking: ";
        for (MenuItem mi : c){
            s += mi.getName() + " ";
        }

        s += "\n";

        setChanged();
        notifyObservers(s);
    }

    @Override
    public void createBill(Order o){
        Collection<MenuItem> c = null;

        for (Order i : orders.keySet()){
            if (o.hashCode() == i.hashCode() && o.getDate().compareTo(i.getDate()) == 0){
                c = orders.get(i);
            }
        }

        String command = "";
        float price = 0.0f;

        for (MenuItem mi : c){
            command += mi.getName() + " " + mi.getPrice() + "\n";
            price += mi.getPrice();
        }

        command += "TOTAL: " + price;

        new FileWriter().write(command);
    }

    public Collection<MenuItem> getOrder(Order o){
        return this.orders.get(o);
    }

    public List<MenuItem> getMenu() {
        return this.menu;
    }

    @Override
    public void add(MenuItem temp) {
        assert temp.getPrice() > 0: "Price less than zero!";
        assert temp != null: "Invalid item!";

        int oldSize = menu.size();

        this.menu.add(temp);

        assert oldSize + 1 == menu.size(): "Element not added!";
    }

    @Override
    public void modify(Object value, int index, int attribute) {
        assert index < menu.size(): "Invalid modify-index!";
        int oldSize = menu.size();

       if (attribute == 0){
           this.menu.get(index).setName((String) value);
       }
       else{
           this.menu.get(index).setPrice(Float.parseFloat((String) value));
       }

       assert oldSize == menu.size(): "Modify operation modified the size of the table!";
    }

    @Override
    public void delete(int id) {
        assert isListEmpty(): "Menu cannot be empty!";
        assert id >= 0: "Invalid delete-id!";

        int oldSize = menu.size();

        assert menu.size() > 1: "Cannot delete last element from the menu!";
        this.menu.remove(id);

        assert oldSize - 1 == menu.size(): "Element not deleted!";
        assert isListEmpty(): "Menu cannot be empty!";
    }


    public boolean isListEmpty(){
        return !(this.menu.size() == 0);
    }

}
