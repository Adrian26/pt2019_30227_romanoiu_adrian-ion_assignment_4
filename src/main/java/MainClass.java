import BusinessLayer.Restaurant;

import PresentationLayer.AdministratorGUI;
import PresentationLayer.ChefGUI;
import PresentationLayer.WaiterGUI;


public class MainClass {

    public static void main(String[] args){
        Restaurant restaurant = new Restaurant();

        ChefGUI chefGUI = new ChefGUI();
        restaurant.addObserver(chefGUI);

        new AdministratorGUI(restaurant, new WaiterGUI(restaurant));

        //a.stream().distinct().map.filter.limit.reduce
    }

}
