package PresentationLayer;

import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel {

    private boolean[][] editableCells;

    public MyTableModel(String[] columnNames, int rows){
        super(columnNames, rows);
        this.editableCells = new boolean[rows][columnNames.length];
    }

    public boolean isCellEditable(int row, int column){
        return this.editableCells[row][column];
    }

    public void setCellEditable(int row, int column, boolean value){
        this.editableCells[row][column] = value;
        this.fireTableCellUpdated(row, column);
    }

}
