package PresentationLayer;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI implements Observer {
    private JFrame mainFrame;

    private JTextArea textArea;

    public ChefGUI(){
        this.mainFrame = new JFrame("Chef");
        this.textArea = new JTextArea();

        textArea.setBackground(Color.GRAY);
        textArea.setForeground(Color.ORANGE);
        textArea.setFont(new Font("Font", Font.BOLD, 20));
        textArea.setEditable(false);

        mainFrame.add(textArea);
        mainFrame.setVisible(true);
        mainFrame.setResizable(false);
        mainFrame.setSize(500, 200);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.textArea.append((String) arg);
        mainFrame.revalidate();
    }
}
