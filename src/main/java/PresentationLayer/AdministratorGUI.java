package PresentationLayer;

import BusinessLayer.*;
import BusinessLayer.MenuItem;
import DataLayer.RestaurantSerialization;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class AdministratorGUI{
    private JFrame mainFrame;

    private Restaurant restaurant;

    private JButton addButton;
    private JButton modifyButton;
    private JButton deleteButton;

    private JTable menuTable;

    private JPanel mainPanel;
    private JScrollPane scrollPane;

    private WaiterGUI waiterGUI;

    public AdministratorGUI(final Restaurant restaurant, WaiterGUI waiterGUI){
        this.waiterGUI = waiterGUI;

        this.restaurant = restaurant;

        mainFrame = new JFrame("Administrator");

        this.addButton    = new JButton("Add");
        this.modifyButton = new JButton("Modify");
        this.deleteButton = new JButton("Delete");

        addButton.addActionListener(new addButtonListener());
        deleteButton.addActionListener(new deleteButtonListener());
        modifyButton.addActionListener(new modifyButtonListener());

        JPanel buttonPanel = new JPanel(new GridLayout(1, 3));
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(100, 0, 100, 0));
        mainPanel = new JPanel(new GridLayout(2, 1));

        buttonPanel.add(addButton);
        buttonPanel.add(modifyButton);
        buttonPanel.add(deleteButton);

        menuTable = createTable(restaurant.getMenu());

        mainPanel.add(buttonPanel);

        scrollPane = new JScrollPane(menuTable);
        mainPanel.add((scrollPane));

        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new RestaurantSerialization().serializate(restaurant.getMenu());
            }
        });

        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        mainFrame.setResizable(false);
        mainFrame.setSize(800, 600);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public JTable createTable(List<MenuItem> menu){
        JTable toReturn = new JTable();

        toReturn.setRowSelectionAllowed(true);
        toReturn.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        String[] columnNames = {"Name", "Price", "Components"};

        MyTableModel tableModel = new MyTableModel(columnNames, menu.size() + 1);
        toReturn.setModel(tableModel);


        for (int i = 0; i < menu.size(); i++){
            String components = "";

            if (menu.get(i) instanceof CompositeProduct){
                components += menu.get(i);
            }
            else{
                components += menu.get(i);
            }


            toReturn.setValueAt(menu.get(i).getName(), i, 0);
            toReturn.setValueAt(menu.get(i).getPrice(), i, 1);
            toReturn.setValueAt(components, i, 2);
        }

        for (int i = 0; i < 3; i++){
            tableModel.setCellEditable(toReturn.getRowCount() - 1, i, true);
        }

        return toReturn;
    }


    private class addButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            int[] selectedRows = menuTable.getSelectedRows();

            if (selectedRows.length < 2){
                    BaseProduct temp = new BaseProduct((String) menuTable.getValueAt(menuTable.getRowCount() - 1, 0),
                            Float.parseFloat((String) menuTable.getValueAt(menuTable.getRowCount() - 1, 1)));

                restaurant.add(temp);
            }
            else{
                MenuItem temp = new MenuItem((String) menuTable.getValueAt(menuTable.getRowCount() - 1, 0));
                for (int i : selectedRows){
                    if (i < restaurant.getMenu().size()){
                        temp.add(restaurant.getMenu().get(i));
                    }
                }

                restaurant.add(temp);
            }

            mainPanel.remove(scrollPane);
            mainPanel.remove(menuTable);

            menuTable = createTable(restaurant.getMenu());
            mainPanel.add(menuTable);
            scrollPane = new JScrollPane(menuTable);
            mainPanel.add(scrollPane);

            mainFrame.revalidate();

            waiterGUI.revalidate();
        }
    }

    private class deleteButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            restaurant.delete(menuTable.getSelectedRow());

            mainPanel.remove(scrollPane);
            mainPanel.remove(menuTable);

            menuTable = createTable(restaurant.getMenu());
            mainPanel.add(menuTable);
            scrollPane = new JScrollPane(menuTable);
            mainPanel.add(scrollPane);

            mainFrame.revalidate();

            waiterGUI.revalidate();
        }
    }

    private class modifyButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Object tableValue1 = menuTable.getValueAt(menuTable.getRowCount() - 1, 0);
            Object tableValue2 = menuTable.getValueAt(menuTable.getRowCount() - 1, 1);

            int row = menuTable.getSelectedRow();

            if (tableValue1 != null){
                restaurant.modify(tableValue1, row, 0);
            }

            if (tableValue2 != null){
                restaurant.modify(tableValue2, row, 1);
            }

            mainPanel.remove(scrollPane);
            mainPanel.remove(menuTable);

            menuTable = createTable(restaurant.getMenu());
            mainPanel.add(menuTable);
            scrollPane = new JScrollPane(menuTable);
            mainPanel.add(scrollPane);

            mainFrame.revalidate();

            waiterGUI.revalidate();
        }
    }

}
