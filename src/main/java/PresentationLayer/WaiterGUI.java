package PresentationLayer;

import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

public class WaiterGUI {

    private JFrame mainFrame;

    private JButton createButton;
    private JButton billButtton;

    private JTable table1;
    private JTable table2;

    private JPanel buttonPanel;
    private JPanel tablePanel;
    private JPanel mainPanel;

    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;

    private Restaurant restaurant;

    public WaiterGUI(Restaurant restaurant){
        this.restaurant = restaurant;

        this.mainFrame = new JFrame("Waiter");

        this.createButton = new JButton("Create Order");
        this.billButtton = new JButton("Create Bill");

        createButton.addActionListener(new createOrderListener());
        billButtton.addActionListener(new createBillListener());

        this.buttonPanel = new JPanel(new GridLayout(1, 2));
        this.buttonPanel.add(createButton);
        this.buttonPanel.add(billButtton);
        this.buttonPanel.setBorder(BorderFactory.createEmptyBorder(100, 0, 100, 0));

        this.mainPanel = new JPanel(new GridLayout(2, 1));
        mainPanel.add(buttonPanel);

        table1 = createTable();
        table2 = showMenu(restaurant.getMenu());

        this.tablePanel = new JPanel(new GridLayout(1, 2));
        tablePanel.add(table1);
        scrollPane1 = new JScrollPane(table1);
        tablePanel.add(scrollPane1);

        tablePanel.add(table2);
        scrollPane2 = new JScrollPane(table2);
        tablePanel.add(scrollPane2);

        mainPanel.add(tablePanel);

        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
        mainFrame.setSize(800, 600);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public JTable createTable(){
        JTable toReturn = new JTable();

        toReturn.setRowSelectionAllowed(true);
        toReturn.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        String[] columnNames = {"OrderID", "TableID", "Date", "Items"};

        DefaultTableModel tableModel = new DefaultTableModel(columnNames,  1);
        toReturn.setModel(tableModel);

        return toReturn;
    }

    private class createOrderListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String orderID = (String) table1.getValueAt(table1.getRowCount() - 1, 0);
            String tableID = (String) table1.getValueAt(table1.getRowCount() - 1, 1);

            Order temp = new Order(Integer.parseInt(orderID), new Date(), Integer.parseInt(tableID));

            int[] selectedRows = table2.getSelectedRows();

            ArrayList<MenuItem> items = new ArrayList<MenuItem>();
            for (int i : selectedRows){
                items.add(restaurant.getMenu().get(i));
            }

            restaurant.createOrder(temp, items);

            table1.setValueAt(temp.getOrderID(), table1.getRowCount() - 1, 0);
            table1.setValueAt(temp.getTableID(), table1.getRowCount() - 1, 1);
            table1.setValueAt(temp.getDate(), table1.getRowCount() - 1, 2);

            String aux = "";
            for (MenuItem mi : restaurant.getOrder(temp)){
                aux += mi.getName() + " ";
            }

            table1.setValueAt(aux, table1.getRowCount() - 1, 3);

            ((DefaultTableModel)table1.getModel()).addRow(new Vector());
        }
    }

    private class createBillListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            int row = table1.getSelectedRow();

            Object name = table1.getValueAt(row, 0);
            Object table = table1.getValueAt(row, 1);
            Object date = table1.getValueAt(row, 2);

            Order temp = new Order((Integer) name, (Date) date,(Integer) table);

            restaurant.createBill(temp);
        }
    }

    public JTable showMenu(List<MenuItem> menu){
        JTable toReturn = new JTable();

        toReturn.setRowSelectionAllowed(true);
        toReturn.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        String[] columnNames = {"Name", "Price", "Components"};

        MyTableModel tableModel = new MyTableModel(columnNames, menu.size());
        toReturn.setModel(tableModel);

        for (int i = 0; i < menu.size(); i++){
            String components = "";

            if (menu.get(i) instanceof CompositeProduct){
                components += menu.get(i);
            }
            else{
                components += menu.get(i);
            }


            toReturn.setValueAt(menu.get(i).getName(), i, 0);
            toReturn.setValueAt(menu.get(i).getPrice(), i, 1);
            toReturn.setValueAt(components, i, 2);
        }

        for (int i = 0; i < 3; i++){
            if (toReturn.getRowCount() > 0){
                tableModel.setCellEditable(toReturn.getRowCount() - 1, i, true);
            }
        }

        return toReturn;
    }

    public void revalidate(){
        tablePanel.remove(scrollPane2);
        tablePanel.remove(table2);

        table2 = showMenu(restaurant.getMenu());
        tablePanel.add(table2);
        scrollPane2 = new JScrollPane(table2);
        tablePanel.add(scrollPane2);

        mainFrame.revalidate();
    }

}
