package DataLayer;

import BusinessLayer.MenuItem;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class RestaurantSerialization {

    public void serializate(List<MenuItem> menuItems){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("Menu.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(menuItems);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<MenuItem> deserializate(){
        List<MenuItem> menuItems = new ArrayList<MenuItem>();

        try {
            FileInputStream fileInputStream = new FileInputStream("Menu.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            menuItems = (List<MenuItem>)objectInputStream.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return menuItems;
    }


}
